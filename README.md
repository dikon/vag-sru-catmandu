# SRU-Daten zum Verlagsarchiv Gebauer-Schwetschke (via Catmandu)

## Benötigte Software

- [Perl](https://www.perl.org/)
    - [Catmandu](https://metacpan.org/release/Catmandu)
    - [Catmandu::SRU](https://metacpan.org/release/Catmandu-SRU)

## Weiterführende Informationen

- Catmandu: [Handbook](https://librecat.org/Catmandu/)
